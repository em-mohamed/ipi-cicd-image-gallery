module.exports = {
  env: {
    es2021: true,
    node: true,
  },
  extends: 'standard-with-typescript',
  overrides: [
    {
      env: {
        node: true,
      },
      files: ['.eslintrc.{js,cjs}'],
      parserOptions: {
        sourceType: 'script',
      },
    },
  ],
  parserOptions: {
    ecmaVersion: 'latest',
    sourceType: 'module',
  },
  rules: {
    semi: ['error', 'always'], // Enforce semicolons
    '@typescript-eslint/semi': ['error', 'always'], // Enforce semicolons in TypeScript
    '@typescript-eslint/no-misused-promises': 'off', // Disable no-misused-promises
    'comma-dangle': ['error', 'always'], // Enforce semicolons
    '@typescript-eslint/comma-dangle': ['error', 'always'], // Enforce semicolons in TypeScript
  },
};
